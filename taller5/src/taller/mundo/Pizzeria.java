package taller.mundo;


public class Pizzeria 
{	
	// ----------------------------------
    // Constantes
    // ----------------------------------
	
	/**
	 * Constante que define la accion de recibir un pedido
	 */
	public final static String RECIBIR_PEDIDO = "RECIBIR";
	/**
	 * Constante que define la accion de atender un pedido
	 */
	public final static String ATENDER_PEDIDO = "ATENDER";
	/**
	 * Constante que define la accion de despachar un pedido
	 */
	public final static String DESPACHAR_PEDIDO = "DESPACHAR";
	/**
	 * Constante que define la accion de finalizar la secuencia de acciones
	 */
	public final static String FIN = "FIN";
	
	// ----------------------------------
    // Atributos
    // ----------------------------------
	
	/**
	 * Heap que almacena los pedidos recibidos
	 */
	// TODO
	private Heap<Pedido> pedidosRecibidos;
	/**
	 * Getter de pedidos recibidos
	 */
	// TODO 

 	/** 
	 * Heap de elementos por despachar
	 */
	// TODO 
	private Heap<Pedido> elementosDespachar;
	/**
	 * Getter de elementos por despachar
	 */
	// TODO 
	
	// ----------------------------------
    // Constructor
    // ----------------------------------

	public Heap<Pedido> getPedidosRecibidos() {
		return pedidosRecibidos;
	}

	public Heap<Pedido> getElementosDespachar() {
		return elementosDespachar;
	}

	/**
	 * Constructor de la case Pizzeria
	 */
	public Pizzeria()
	{
		// TODO 
		pedidosRecibidos = new Heap<>(25);
		elementosDespachar = new Heap<>(251);
	}
	
	// ----------------------------------
    // Métodos
    // ----------------------------------
	
	/**
	 * Agrega un pedido a la cola de prioridad de pedidos recibidos
	 * @param nombreAutor nombre del autor del pedido
	 * @param precio precio del pedido 
	 * @param cercania cercania del autor del pedido 
	 */
	public void agregarPedido(String nombreAutor, double precio, int cercania)
	{
		// TODO 
		Pedido nuevoPedido = new Pedido(precio, nombreAutor, cercania);
		pedidosRecibidos.add(nuevoPedido);
	}
	
	// Atender al pedido más importante de la cola
	
	/**
	 * Retorna el proximo pedido en la cola de prioridad o null si no existe.
	 * @return p El pedido proximo en la cola de prioridad
	 */
	public Pedido atenderPedido()
	{
		// TODO 
		Pedido atender = pedidosRecibidos.poll();
		atender.setDespachado();
		elementosDespachar.add(atender);
		return  atender;
	}

	/**
	 * Despacha al pedido proximo a ser despachado. 
	 * @return Pedido proximo pedido a despachar
	 */
	public Pedido despacharPedido()
	{
		// TODO 
		
	    return  elementosDespachar.poll();
	}
	
	 /**
     * Retorna la cola de prioridad de pedidos recibidos como un arreglo. 
     * @return arreglo de pedidos recibidos manteniendo el orden de la cola de prioridad.
     */
     public Pedido [] pedidosRecibidosList()
     {
        // TODO 
        return  pedidosRecibidos.getPedidos();
     }
     
      /**
       * Retorna la cola de prioridad de despachos como un arreglo. 
       * @return arreglo de despachos manteniendo el orden de la cola de prioridad.
       */
     public Pedido [] colaDespachosList()
     {
         // TODO 
         return  elementosDespachar.getPedidos();
     }
}
