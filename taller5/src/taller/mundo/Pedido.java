package taller.mundo;

public class Pedido implements Comparable<Pedido>
{

	// ----------------------------------
	// Atributos
	// ----------------------------------

	/**
	 * Precio del pedido
	 */
	private double precio;
	
	/**
	 * Autor del pedido
	 */
	private String autorPedido;
	
	/**
	 * Cercania del pedido
	 */
	private int cercania;
	
	
	private boolean despachado;
	
	// ----------------------------------
	// Constructor
	// ----------------------------------
	

	/**
	 * Constructor del pedido
	 * TODO Defina el constructor de la clase
	 */
	public Pedido (double pPrecio, String pAutor, int pCercania)
	{
		precio = pPrecio;
		autorPedido = pAutor;
		cercania = pCercania;
	}
	// ----------------------------------
	// Métodos
	// ----------------------------------
	
	public boolean isDespachado() {
		return despachado;
	}

	public void setDespachado() {
		this.despachado = !despachado;
	}

	/**
	 * Getter del precio del pedido
	 */
	public double getPrecio()
	{
		return precio;
	}
	
	/**
	 * Getter del autor del pedido
	 */
	public String getAutorPedido()
	{
		return autorPedido;
	}
	
	/**
	 * Getter de la cercania del pedido
	 */
	public int getCercania() {
		return cercania;
	}



	@Override
	public int compareTo(Pedido o) {
		// TODO Auto-generated method stub
		int respuesta = 0;
		if(despachado)
		{
		if(o.getCercania() < this.getCercania())
			respuesta = 1;
		else if(o.getCercania() > this.getCercania())
			respuesta = -1;
		}
		else
		{
			if(o.getPrecio() < this.getPrecio())
				respuesta = 1;
				else if(o.getPrecio() > this.getPrecio())
					respuesta = -1;
		}
		return respuesta;
	}
	

	
	// TODO 
}
