package taller.mundo;

import taller.estructuras.IHeap;

public class Heap<T extends Comparable<T>> implements IHeap{
	
	private T[] pedidos;
	private int N;
	/*
	 * tipo del arbol
	 * True si es para el precio, false para la cercania
	 */

	public Heap(int maxN)
	{
		pedidos = (T[]) new Pedido[maxN+1];
	}

	@Override
	public void add(Object elemento) {
		pedidos[++N] = (T) elemento;
		siftUp();
	}
	
	public T peek() {
		return pedidos[1];
	}

	@Override
	public T poll() {
		T resp = pedidos[1];
		T temp = pedidos[1];
		exch( 1, N--);
		pedidos[N+1] = null;
		siftDown();
		return resp;
		
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return N;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return N == 0;
	}

	@Override
	public void siftUp() {
		// TODO Auto-generated method stub
		int k = N;
		while( k > 1 && pedidos[k/2].compareTo(pedidos[k]) < 0)
		{
			exch(k/2, k);
			k = k/2;
		}

	}


	public T[] getPedidos() {
		return pedidos;
	}

	@Override
	public void siftDown() {
		// TODO Auto-generated method stub
		int k = 1;
		while(2*k <= N)
		{
		int j = 2*k;
		if(j < N && pedidos[j].compareTo(pedidos[j+1]) < 0) j++;
		if(!(pedidos[k].compareTo(pedidos[j]) < 0)) break;
		exch(k,j);
		k = j;
		}
		
	}
	private void exch(int i , int k)
	{
		T t = pedidos[i];
		pedidos[i] = pedidos[k];
		pedidos[k] = t;
	}


}
